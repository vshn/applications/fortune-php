# Step 1: Composer image
FROM php:8.2-fpm-alpine AS composer
RUN apk update && apk add --no-cache zip php-zip
WORKDIR /app
COPY composer.json .
COPY composer.lock .
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN php composer.phar install --no-interaction --optimize-autoloader

# tag::production[]
# Step 2: Production image
# Adapted from
# https://github.com/TrafeX/docker-php-nginx
FROM alpine:3.17

WORKDIR /var/www/html

# Install packages and remove default server definition
RUN apk --no-cache add fortune nginx php81 php81-fpm php81-json supervisor

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/conf.d /etc/nginx/conf.d/

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php81/php-fpm.d/www.conf
COPY config/php.ini /etc/php81/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html && mkdir -p /run/ && mkdir -p /var/cache/nginx

# The following lines make this image compatible with OpenShift.
# Source: https://torstenwalter.de/openshift/nginx/2017/08/04/nginx-on-openshift.html
RUN chmod g+rwx /var/cache/nginx /run /var/log/nginx

# Add application
COPY --chown=1001:0 templates /var/www/html/templates
COPY --chown=1001:0 public /var/www/html/public
COPY --from=composer --chown=1001:0 /app/vendor /var/www/html/vendor

# Make sure files/folders needed by the processes are accessable when they run under the 1001 user
RUN chown -R 1001:0 /var/www/html /run /var/lib/nginx /var/log/nginx

# Expose the port nginx is reachable on
EXPOSE 8080

# Switch to use a non-root user from here on
# <1>
USER 1001:0

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
# end::production[]
