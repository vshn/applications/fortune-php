<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();
$twig = Twig::create('../templates');
$app->add(TwigMiddleware::create($app, $twig));

$version = '1.2-php';
$hostname = `hostname`;

// tag::router[]
$app->get('/', function (Request $request, Response $response, $args) use ($version, $hostname) {
    $data = [
        'message' => `fortune`,
        'number' => rand(1, 1000),
        'version' => $version,
        'hostname' => $hostname
    ];
    if ($request->hasHeader('Accept')) {
        $acceptHeader = $request->getHeaderLine('Accept');
        if ($acceptHeader === 'application/json') {
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withHeader('Content-Type', 'application/json');
        }
        if ($acceptHeader === 'text/plain') {
            $format = "Fortune %s cookie of the day #%d:\n\n%s";
            $result = sprintf($format, $data['version'], $data['number'], $data['message']);
            $response->getBody()->write($result);
            return $response->withHeader('Content-Type', 'text/plain');
        }
    }
    $view = Twig::fromRequest($request);
    return $view->render($response, 'fortune.html', $data);
});
// end::router[]

$app->run();
